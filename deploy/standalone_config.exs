import Config

config :beng, :listen, [port: 4001]

config :beng, Beng.Repo,
  database: "/srv/beng/data/comments.sqlite3"
