import Config

config :beng, ecto_repos: [Beng.Repo]

config :beng, Beng.Repo,
  adapter: Ecto.Adapters.Sqlite

config :beng, Beng.Mailer,
  adapter: Bamboo.SMTPAdapter
