defmodule Comment do
  use Ecto.Schema

  schema "comments" do
    # id field is implicit
    field :name, :string
    field :content, :string
    field :approved, :boolean

    timestamps()
  end
end

defmodule Notification do
  use Ecto.Schema

  schema "notified_about" do
    # id field is implicit
    # creation time of the last comment
    # that was made known to the admin
    field :last_time, :naive_datetime
  end
end
